<?php

namespace ESIeasy;

class EsiAPI{

    protected $authToken = null;
    protected $esi_path = "https://esi.evetech.net/latest/";
    protected $client; 
    protected $lastresponse;
    protected $headers = ["Content-Type"=>'application/json','Accept'=>'application/json'];



	public function __construct($token=null){
		// Constructor
		if($token != null){
			$this->setAuthToken($token);
        }

        $this->client = new \GuzzleHttp\Client(['base_uri'=>$this->esi_path,]);
	}

	public function setAuthToken($token){
		$this->authToken = $token;
    }


    // This function converts curly braces in the URI to the parameters from the data. Ripped direct from EveSeat\Eseye as it worked well.
    private function mapDataToUri(string $uri, array $data): string {
        // Find the list of elements within curly braces in the uri string
        if (preg_match_all('/{+(.*?)}/',$uri,$matches)){
            // If there are no parameters passed, then what are we doing here, 
            if(empty($data)){
                throw new Exception('No parameters passed');
            }

            // Run through each of the matches, and then replace the URI string with the parameter value.
            foreach($matches[1] as $match){
                if(!array_key_exists($match,$data)){
                    throw new Exception('Data missing from parameters passed');
                }
                // Perform the substitution and at the same time remove any leading / from the uri
                $uri=str_replace('{' . $match . '}',$data[$match],ltrim($uri,'/'));
            }

        }
        return $uri;
    }

    public function invoke($method,$uri,$params,$auth=true){
        // This is the core function, which handles most of the queries.
        $uri_final = $this->mapDataToUri($uri,$params);

        // Set the headers for the auth process if we need to authenticate - default is yes
        if ($auth){
            $headers = array_merge(['Authorization'=>'Bearer ' . $this->authToken->getAccessToken()],$this->headers);
        } else {
            $headers = $this->headers;
        }

        $headers = ['headers'=>$headers];

        // If the parameters contains a body value
        if(array_key_exists('body',$params)){
            $body = $params['body'];
            
        }

        $this->lastresponse = $this->client->request($method,$uri_final,$headers,$body);
        if($this->lastresponse->getStatusCode()==200){
            // The request was successful
            return json_decode($this->lastresponse->getBody());
        }
        return $this->lastresponse;
    }

}


?>
