<?php

namespace ESIeasy;


class AuthToken{

	protected $access_token = null;
	protected $expires_at = null;
	protected $refresh_token = "empty";
	protected $client_id = "empty";
	protected $client_secret = "empty";
	protected $auth_url = 'https://login.eveonline.com/oauth/token';

	public function __construct($token = null){
		// Constructor
		// We may have environment variables already set, so if that is the case then use those
		// to populate the client ID and security to simplify workflow.
		$this->client_id = getenv('EVEONLINE_CLIENT_ID');
        $this->client_secret = getenv('EVEONLINE_CLIENT_SECRET');
        if ($token != null){
            $this->refresh_token = $token;
        }
	}

	/*
	 * Set the app security - this is the id and secret as defined on developers.eveonline.com
	 */
	public function setAppSecurity($app_id,$app_secret){
		$this->client_id = $app_id;
		$this->client_secret = $app_secret;
	}

    // Set just the refresh token
	public function setRefreshToken($token){
        // Set the refresh token and reset the expires_at timer as we will need to re-exchange tokens most likely.
        $this->refresh_token = $token;
        $this->expires_at = null;
	}

    // Take all three parameters and set those codes.
	public function setAllAuth($app_id,$app_secret,$refresh_token){
		$this->setAppSecurity($app_id,$app_secret);
		$this->setRefreshToken($refresh_token);
	}

    // This function handles the flow for converting a refresh token into an access token if the existing auth token has expired.
    public function exchangeToken(){
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST',$this->auth_url,['auth'=>[$this->client_id,$this->client_secret],'query'=>['grant_type'=>'refresh_token','refresh_token'=>$this->refresh_token]]);
        if($response->getStatusCode()==200){
            $body = json_decode($response->getBody());
            $expiry = \Carbon\Carbon::now();
            $expiry->addSeconds($body->expires_in);
            $this->expires_at = $expiry;
            $this->access_token = $body->access_token;
        } else {
            $this->expires_at = null;
            $this->access_token = null;
        }
    }

    // Return the most recent access token - this is a stub for exchangeToken effectively but we may implement caching at some point, so this is an advance thought around that.
    public function getAccessToken(){
        // This is the main bit of code which handles the access token element - we need to check to see if the token is valid.
        if($this->expires_at == null || \Carbon\Carbon::now() >= $this->expires_at){
            // the token has expired so we need to re-exchange it same if the token was not set yet.
            $this->exchangeToken();
        }
        // Now we can return just the access token
        return $this->access_token;
    }

    // Return the client ID and secrets for the application - this is possibly needed by the main EsiAPI class at some point, unsure just yet however.
    public function getClientID(){
        return $this->client_id;
    }

    public function getClientSecret(){
        return $this->client_secret;
    }
}

?>
