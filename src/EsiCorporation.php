<?php

namespace ESIeasy;

class EsiCorporation{


    protected $api;
    protected $corpid;

	public function __construct($api,$corpid){
        // Constructor
        $this->setAPI($api);
        $this->setCorp($corpid);
    }

    /*
     * Update the API and corp information - 
     */
    public function setAPI($api){
        $this->api=$api;
    }

    public function setCorp($corpid){
        $this->corpid=$corpid;
    }

    public function info(){
        return $api->invoke('GET','/corporations/{corpid}/',['corpid'=>$this->corpid],false);
    }

    public function members(){
        return $api->invoke('GET','/corporations/{corpid}/members/',[]);
    }

    public function memberTracking(){
        return $api->invoke('GET','/corporations/{corpid}/membertracking/',['corpid'=>$this->corpid]);
    }

    public function structures(){
        return $api->invoke('GET','/corporations/{corpid}/structures/',['corpid'=>$this->corpid]);
    }

    public function titles(){
        return $api->invoke('GET','/corporations/{corpid}/titles/',['corpid'=>$this->corpid]);
    }

    public function blueprints(){
        return $api->invoke('GET','/corporations/{corpid}/blueprints/',['corpid'=>$this->corpid]);
    }

    public function allianceHistory(){
        return $api->invoke('GET','/corporations/{corpid}/alliancehistory/',['corpid'=>$this->corpid]);
    }

    public function containerLogs(){
        return $api->invoke('GET','/corporations/{corpid}/containers/logs/',['corpid'=>$this->corpid]);
    }

    public function divisions(){
        return $api->invoke('GET','/corporations/{corpid}/divisions/',['corpid'=>$this->corpid]);
    }

    public function facilities(){
        return $api->invoke('GET','/corporations/{corpid}/facilities/',['corpid'=>$this->corpid]);
    }

    public function icons(){
        return $api->invoke('GET','/corporations/{corpid}/icons/',['corpid'=>$this->corpid]);
    }

    public function medals(){
        return $api->invoke('GET','/corporations/{corpid}/medals/',['corpid'=>$this->corpid]);
    }

    public function medalsIssued(){
        return $api->invoke('GET','/corporations/{corpid}/medals/issued/',['corpid'=>$this->corpid]);
    }

    public function memberTitles(){
        return $api->invoke('GET','/corporations/{corpid}/members/titles/',['corpid'=>$this->corpid]);
    }

    public function rolesHistory(){
        return $api->invoke('GET','/corporations/{corpid}/roles/history/',['corpid'=>$this->corpid]);
    }

    public function shareholders(){
        return $api->invoke('GET','/corporations/{corpid}/shareholders/',['corpid'=>$this->corpid]);
    }

    public function standings(){
        return $api->invoke('GET','/corporations/{corpid}/standings/',['corpid'=>$this->corpid]);
    }

    public function starbases(){
        return $api->invoke('GET','/corporations/{corpid}/starbases/',['corpid'=>$this->corpid]);
    }

    public function starbaseDetail($starbase){
        return $api->invoke('GET','/corporations/{corpid}/starbases/{starbase_id}/',['corpid'=>$this->corpid,'starbase_id'=>$starbase]);
    }

    public function  (){
        return $api->invoke('GET','/corporations/{corpid}//',['corpid'=>$this->corpid]);
    }



    public function  (){
        return $api->invoke('GET','/corporations/{corpid}//',['corpid'=>$this->corpid]);
    }



}


?>
